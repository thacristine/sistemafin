<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id',
        'race_id',
        'algorithm_id',
        'date',
        'credits',
        'position',
        'odds',
        'betwin',
        'wincredits',
        'housecredits',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    } 
    
    public function betscore(){
        return $this->hasOne('App\BetScore');
    } 
}