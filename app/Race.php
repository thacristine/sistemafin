<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'cost',
        'gain',
        'algorithm_id',
        'user_id',
    ];

    public function algorithm(){
        return $this->belongsToMany('App\Algorithm'); 
    }

    public function racescore(){
        return $this->hasMany('App\RaceScore', 'race_id');
    }

    public function bet(){
        return $this->hasMany('App\Bet', 'race_id');

    }


}
