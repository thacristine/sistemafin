<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Algorithm extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'betfeatures',
        'editdate',
        'valid',
        'raceready',
        'runoutput',
        'program',
        'user_id',
    ];

    public function race(){
        return $this->belongsToMany('App\Race'); 
    }

    public function user(){
        return $this->belongsTo('App\User');
    }  
}
