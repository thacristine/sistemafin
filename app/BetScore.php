<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class BetScore extends Model
{

    protected $table = 'betscores';
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'position',
        'seconds',
        'profits',
        'minprofits',
        'maxprofits',
        'trades',
        'drawdown',
        'plnot',
        'points',
        'algorithm_id',
        'user_id',
    ];

    public function algorithm(){
        return $this->belongsTo('App\Algorithm');
    } 

    public function user(){
        return $this->belongsTo('App\User');
    } 
}