<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Race;
use App\RaceScore;
use App\Algorithm;
use App\Bet;
use Carbon\Carbon;

class GambleController extends Controller
{
    public function index(){
        return redirect()->route('gamble.bet');
    }
    
    public function bet(){

        //$races = Race::with('user')->with('algorithm')->whereBetween('date', [Carbon::today('Europe/London'),Carbon::tomorrow('Europe/London')])->paginate();
        $algorithms = Algorithm::paginate();
        $today = Carbon::now('Europe/London');
        return view('gamble.bet', compact('algorithms', 'today'));
    }

    public function winners(){
        
        $races = Race::with('bet')->paginate();
        
        // Algorithm owner
        //dd($races[0]->algorithm[0]->user->name);

        // Algorithm name
        //dd($races[0]->algorithm[0]->name);

        // Algorithm position in the race
        //dd($races[1]->score[1]->position);

        // User who bet in the race
        //dd($races[0]->bet[0]->user->name);
        //dd($races[1]->bet[0]->score);

        return view('gamble.winners', compact('races'));
    }
}
