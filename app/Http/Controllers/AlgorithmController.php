<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Algorithm;
use Auth;

class AlgorithmController extends Controller
{
    public function index() {
        return redirect()->route('garage.list');
    }
    public function list(){

        $id = Auth::user()->id;
        $algorithms = Algorithm::where('user_id', '=', $id)->get();
        
        return view('garage.list', compact('algorithms'));
    }

    public function run($id){
        return view('garage.run');
    }

    public function design(){
        return view('garage.design');
    }
}