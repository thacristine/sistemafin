<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Race;
use App\RaceScore;
use App\User;

class RaceController extends Controller
{
    public function index(){

        ///$results = Race::with('algorithm')->find(735);
        //$results = Race::with('algorithm')->orderBy('created_at')->paginate(10);

        $races = Race::with('racescore')->paginate();
        
        //dd($races[0]->racescore[0]);

        return view('race.index', compact('races'));
    }

    public function profiles() {
        $users = User::all();
        return view('race.club', compact('users'));
    }
}
