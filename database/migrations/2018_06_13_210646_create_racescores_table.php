<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRacescoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('racescores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('gain')->nullable();
			$table->integer('profits')->nullable();
			$table->integer('minprofits')->nullable();
			$table->integer('maxprofits')->nullable();
			$table->integer('trades')->nullable();
			$table->integer('race_id')->index('fk_scores_races1_idx');
			$table->decimal('seconds', 10, 0)->nullable();
			$table->integer('drawdown')->nullable();
			$table->integer('plnot')->nullable();
			$table->integer('points')->nullable();
			$table->integer('position')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('racescores');
	}

}
