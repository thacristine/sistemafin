<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlgorithmRaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('algorithm_race', function(Blueprint $table)
		{
			$table->integer('race_id')->index('fk_algorithmready_races1_idx');
			$table->integer('algorithm_id')->index('fk_algorithm_race_algorithms1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('algorithm_race');
	}

}
