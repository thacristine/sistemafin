<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBetscoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('betscores', function(Blueprint $table)
		{
			$table->foreign('bet_id', 'fk_scores_bets1')->references('id')->on('bets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('betscores', function(Blueprint $table)
		{
			$table->dropForeign('fk_scores_bets1');
		});
	}

}
