<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlgorithmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('algorithms', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 60)->nullable();
			$table->text('betfeatures', 65535)->nullable();
			$table->dateTime('editdate')->nullable();
			$table->integer('valid')->nullable()->default(0);
			$table->integer('raceready')->default(0);
			$table->text('runoutput', 65535)->nullable();
			$table->text('program', 65535);
			$table->timestamps();
			$table->integer('user_id')->unsigned()->index('fk_algorithms_users_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('algorithms');
	}

}
