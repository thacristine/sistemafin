<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bets', function(Blueprint $table)
		{
			$table->foreign('race_id', 'fk_bets_races1')->references('id')->on('races')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'fk_bets_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bets', function(Blueprint $table)
		{
			$table->dropForeign('fk_bets_races1');
			$table->dropForeign('fk_bets_users1');
		});
	}

}
