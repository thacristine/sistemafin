<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->integer('credits')->nullable();
			$table->string('username', 45)->nullable();
			$table->string('datasource', 45)->nullable()->default('datasource');
			$table->string('language', 45)->nullable();
			$table->string('loadip', 45)->nullable();
			$table->integer('rowsinlist')->nullable();
			$table->dateTime('loaddate')->nullable();
			$table->integer('emailverified')->nullable();
			$table->string('phonenumber', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
