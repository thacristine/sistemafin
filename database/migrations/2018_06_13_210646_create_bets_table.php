<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('algorithm_id');
			$table->dateTime('date')->nullable();
			$table->decimal('credits', 9, 0)->nullable()->default(0);
			$table->integer('position')->nullable()->default(1);
			$table->float('odds', 10, 0)->nullable()->default(1);
			$table->integer('betwin')->nullable()->default(0);
			$table->decimal('wincredits', 9, 0)->nullable()->default(0);
			$table->decimal('housecredits', 9, 0)->nullable()->default(0);
			$table->string('created_at', 45)->nullable();
			$table->string('updated_at', 45)->nullable();
			$table->integer('user_id')->unsigned()->index('fk_bets_users1_idx');
			$table->integer('race_id')->index('fk_bets_races1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bets');
	}

}
