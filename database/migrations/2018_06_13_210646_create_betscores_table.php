<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBetscoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('betscores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('gain')->nullable();
			$table->integer('profits')->nullable();
			$table->integer('minprofits')->nullable();
			$table->integer('maxprofits')->nullable();
			$table->integer('trades')->nullable();
			$table->decimal('seconds', 10, 0)->nullable();
			$table->integer('drawdown')->nullable();
			$table->integer('plnot')->nullable();
			$table->integer('points')->nullable();
			$table->integer('position')->nullable();
			$table->timestamps();
			$table->integer('bet_id')->index('fk_scores_bets1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('betscores');
	}

}
