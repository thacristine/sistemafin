<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRacescoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('racescores', function(Blueprint $table)
		{
			$table->foreign('race_id', 'fk_scores_races10')->references('id')->on('races')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('racescores', function(Blueprint $table)
		{
			$table->dropForeign('fk_scores_races10');
		});
	}

}
