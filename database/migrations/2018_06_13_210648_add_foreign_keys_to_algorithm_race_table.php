<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAlgorithmRaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('algorithm_race', function(Blueprint $table)
		{
			$table->foreign('algorithm_id', 'fk_algorithm_race_algorithms1')->references('id')->on('algorithms')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('race_id', 'fk_algorithmready_races1')->references('id')->on('races')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('algorithm_race', function(Blueprint $table)
		{
			$table->dropForeign('fk_algorithm_race_algorithms1');
			$table->dropForeign('fk_algorithmready_races1');
		});
	}

}
