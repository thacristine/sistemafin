<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(['middleware' => ['role:superadministrator|administrator|user']], function() {
    
    Route::get('/', function () {
        return view('users.index');
    });

    Route::group(['prefix' => 'garage'], function() {
        Route::get('/', 'AlgorithmController@index');
        Route::get('/list', 'AlgorithmController@list')->name('garage.list');
        Route::post('/run/{id}', 'AlgorithmController@run')->name('garage.run');
        Route::get('/design', 'AlgorithmController@design')->name('garage.design');
    });
    Route::get('/users/credits', 'UserController@credits')->name('users.credits');
    Route::resource('/users', 'UserController');

    Route::group(['prefix' => 'race'], function() {
        Route::get('/', 'RaceController@index')->name('race.index');
        Route::get('/profiles', 'RaceController@profiles')->name('race.profiles');
    });

    Route::group(['prefix' => 'gamble'], function() {
        Route::get('/', 'GambleController@index');
        Route::get('/bet', 'GambleController@bet')->name('gamble.bet');
        Route::get('/winners', 'GambleController@winners')->name('gamble.winners');
    });
});

Route::get('/home', 'HomeController@index');
Route::get('/contact', 'Controller@contactus')->name('contact');

Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    // Password reset
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});