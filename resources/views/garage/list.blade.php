@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height bg bg-garage">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-20">
                <div class="jumbotron">
                    <div class="container">
                        <h3>Algorithm List</h3>
                    </div>
                </div>
                <div class="panel panel-list text-center">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Date</th>
                                    <th>Algorithm</th>
                                    <th>Size</th>
                                    <th>Valid</th>
                                    <th>Output</th>
                                    <th>Race ready</th>
                                    <th>Run test</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($algorithms as $algorithm)
                                    <tr>
                                        <td>Archive</td>
                                        <td>{{ $algorithm->created_at }}</td>
                                        <td>{{ $algorithm->name }}</td>
                                        <td>7434</td>
                                        <td>
                                            @if($algorithm->valid == 2)
                                                <figure class="circle bg-green"></figure>
                                            @else
                                                <figure class="circle bg-red"></figure>
                                            @endif
                                        </td>
                                        <td>Main(05-19 | XXXX | STO1) => PnL = 12.475 [-322.92</td>
                                        <td>
                                            @if($algorithm->raceready == 0)
                                                <figure class="circle bg-red"></figure>
                                            @else
                                                <figure class="circle bg-green"></figure>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('garage.run', $algorithm->id) }}" class="btn btn-default" aria-label="Left Align">Run test</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <p>
                            Only you can see your algorithms.<br>
                            Run test algorithm takes several seconds to complete the scala program. Wait until your browser has loaded the page.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

   