@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height bg bg-garage">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-20">
                <div class="jumbotron">
                    <div class="container">
                        <h3>Design algorithm</h3>
                    </div>
                </div>
                <div class="panel panel-list col-md-6 col-md-offset-3">
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">File: *.scala</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            <div class="form-group">
                                <span>max 2Mb (8Mb)</span>
                            </div>
                            
                            <button class="btn btn-default">Ok</button>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

   