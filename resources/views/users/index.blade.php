@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height bg bg-users">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 m-t-20">
                <div class="jumbotron">
                    <div class="container">
                        <h3>Profile</h3>
                    </div>
                </div>
                <div class="panel panel-list text-center col-md-12">
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="col-md-3">Full name</td>
                                    <td>{{ Auth::user()->name }}</td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td>{{ Auth::user()->username }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ Auth::user()->email }}</td>
                                </tr>
                                <tr>
                                    <td>Race club</td>
                                    <td>{{ Auth::user()->raceclub }}</td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td>*******</td>
                                </tr>
                                <tr>
                                    <td>Language</td>
                                    <td>{{ Auth::user()->language }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                
                <div class="content">
                    <h2>Profile</h2>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

   