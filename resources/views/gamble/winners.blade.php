@extends('layouts.app')

@section('content')
<div class="bg-gamble bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-20">
                <div class="jumbotron">
                    <div class="container">
                        <h3>Winners List</h3>
                    </div>
                </div>
                <div class="panel panel-list text-center col-md-12">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Race date</th>
                                    <th>Race position</th>
                                    <th>Racer</th>
                                    <th>Algorithm</th>
                                    <th>Bet date</th>
                                    <th>Gambler</th>
                                    <th>Credits</th>
                                    <th>Bet position</th>
                                    <th>Win Odds</th>
                                    <th>Win credits</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($races as $race)
                                    @foreach($race->bet as $bet)    
                                        <tr>
                                            <td>{{ $race->date }}</td>
                                            <td>{{ $bet->betscore->position }}</td>
                                            <td>{{ $bet->user->username }}</td>
                                            <td>{{ $race->algorithm[0]->name }}</td>
                                            <td>{{ $bet->date }}</td>
                                            <td>{{ $race->algorithm[0]->user->username }}</td>
                                            <td>{{ $bet->credits }}</td>
                                            <td>{{ $bet->position }}</td>
                                            <td>{{ $bet->odds }}</td>
                                            <td>{{ $bet->wincredits }}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel col-md-12">
                    <div class="panel-body">
                        <div class="container">
                            <p>
                                You can see the winners list of your race club.
                                <br>When betting on win, place or show are calculated in their own bet pool.
                                <br>All bet pools must have at least 2 users betting.
                                <br>If bet pool has only 1 user then the bet credits are returned to user.
                                <br>Users get 90 % of bet credits.
                                <br>House gets 10 % of bet credits.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

   