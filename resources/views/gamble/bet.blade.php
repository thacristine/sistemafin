@extends('layouts.app')

@section('content')
<div class="bg-gamble bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-20">
                <div class="jumbotron">
                    <div class="container">
                        <h3>Bet algorithms</h3>
                    </div>
                </div>
                <div class="panel panel-list text-center col-md-12">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Race date</th>
                                    <th>Racer</th>
                                    <th>Algorithm</th>
                                    <th>Bet date</th>
                                    <th>Credits</th>
                                    <th>Position</th>
                                    <th>Odds</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($algorithms as $algorithm)
                                    @if($algorithm->raceready == 1)
                                    <tr>
                                        <td>{{ $today }}</td>
                                        <td>{{ $algorithm->user->username }}</td>
                                        <td>{{ $algorithm->name }}</td>
                                        <td>today</td>
                                        <td>0</td>
                                        <td></td>
                                        <td>-</td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel col-md-12">
                    <div class="panel-body">
                        <div class="container">
                            <p>
                                You can bet algorithms of your race club.
                                <br>These are algorithms that are race ready and you can bet on algorithms.
                                <br>Race competition is run every night.
                                <br>No more bets after midnight.
                                <br>Add your bets by clicking the link in the credits-column.
                                <br>Bet date is when you made your bet or current date and time.
                                <br>You can see only your bets.
                                <br>Everybody's bettings change the odds in the algorithm.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

   