@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center m-t-20">
            <div class="panel panel-default">
                <div class="panel-heading">Contact Us</div>

                <div class="panel-body center">
                    <h2>Rayleigh Research Oy</h2>
                    <p>
                        Address	Henry Fordin katu 5 C <br>
                        00150 Helsink, FI  
                        +358 xxxx xxxx <br>
                        info@rayleighresearch.com <br>
                        Contact person	<br>
                        Tommi A. Vuorenmaa  
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
