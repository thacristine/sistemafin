@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height bg bg-race">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-20">
                <div class="jumbotron">
                    <div class="container">
                        <h3>Race Results</h3>
                    </div>
                </div>
                <div class="panel panel-list text-center">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Race date</th>
                                    <th>Racer</th>
                                    <th>Race club</th>
                                    <th>Algorithm</th>
                                    <th>Seconds</th>
                                    <th>Profits</th>
                                    <th>Profits min</th>
                                    <th>Profits max</th>
                                    <th>Trades</th>
                                    <th>Race cost</th>
                                    <th>Position</th>
                                    <th>Race gain</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($races as $race)
                                    @foreach($race->racescore as $score)
                                        <tr>
                                            <td>{{ date('d/m/Y', strtotime($race->created_at )) }}</td>
                                            <td>{{ $race->algorithm[0]->user->name }}</td>
                                            <td>{{ $race->algorithm[0]->user->raceclub }}</td>
                                            <td>{{ $race->algorithm[0]->name }}</td>
                                            <td>{{ $score->seconds }}</td>
                                            <td>{{ $score->profits }}</td>
                                            <td>{{ $score->minprofits }}</td>
                                            <td>{{ $score->maxprofits }}</td>
                                            <td>{{ $score->trades }}</td>
                                            <td>{{ $race->cost }}</td>
                                            <td>{{ $score->position }}</td>
                                            <td>{{ $score->gain }}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                        <nav aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <p>
                            You can list race results of your race club.

                            <br>A race costs RAR credits for the user that developed the algorithm.
                            
                            <br>Winning of a race gives race gain for the 3 users.
                            
                            <br>Race position is the order of profits.
                            
                            <br>House gets 10 % of credits for arranging the race competition.
                            
                            <br>Users get return 90 % of credits.
                            
                            <br>First algorithm get 50% of return credits.
                            
                            <br>Second algorithm get 35% of return credits.
                            
                            <br>Third algorithm get 25% of return credits.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

   