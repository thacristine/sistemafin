@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height bg bg-race">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-20">
                <div class="jumbotron">
                    <div class="container">
                        <h3>Race Club</h3>
                    </div>
                </div>
                <div class="panel panel-list text-center col-md-6 col-md-offset-3">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Racer</th>
                                    <th>Race club</th>
                                    <th>Credits</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->raceclub }}</td>
                                        <td>{{ $user->credits }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel col-md-6 col-md-offset-3">
                    <div class="panel-body">
                        <div class="container">
                            <p>Your algorithm competes against these algorithms in the race.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

   