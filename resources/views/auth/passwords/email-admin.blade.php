@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 m-t-100">
            <div class="panel panel-default">
                <div class="panel-heading">ADMIN Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('admin.password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-5 m-t-10">
                                <button type="submit" class="btn btn-auth">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <hr>
            
        <div class="footer-login col-md-9 col-md-push-4">
            <a class="btn btn-link" href="{{ route('login') }}">
                Log In
            </a>
            <a class="btn btn-link" href="{{ route('register') }}">
                Not registered yet?
            </a>
        </div>
    
        <hr>
    </div>
</div>
@endsection
