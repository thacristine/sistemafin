@extends('layouts.app')

@section('content')
<div class="container bg">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 m-t-20 ">
            <!-- test form
                <form>
                    <label>
                        <input type="text" required />
                        <div class="label-text">First name</div>
                    </label>
                    <label>
                        <input type="text" required />
                        <div class="label-text">Last name</div>
                    </label>
                    <button>Submit</button>
                </form>-->
            <div class="panel panel-default">

                <div class="panel-body">
                    <img src="images/logo.png">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {{-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> --}}

                            {{-- <div class="col-md-9 col-md-push-2"> --}}
                            <div class="col-md-12">
                                <input id="email" placeholder="EMAIL" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {{-- <label for="password" class="col-md-4 control-label">Password</label> --}}

                            <div class="col-md-12">
                                <input id="password" placeholder="PASSWORD" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
{{--}}
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>--}}

                        <div class="form-group">
                            <div class="col-md-3 col-sm-push-9 m-t-20">
                                <button type="submit" class="btn btn-auth">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <hr>
            
        <div class="footer-login col-md-9 col-md-push-4">
            <a class="btn btn-link" href="{{ route('register') }}">
                Not registered yet?
            </a>
            <a class="btn btn-link" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>
        </div>
        
        <hr>

    </div>
</div>
@endsection
