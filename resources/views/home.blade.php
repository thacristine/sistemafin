@extends('layouts.app')

@section('content')
<div class="container bg bg-users">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 m-t-20">
            <div class="panel panel-default">{{--}}
                <div class="panel-heading">Users Dashboard</div>

                <div class="panel-body">
                    @component('components.who')
                    @endcomponent
                </div>--}}
            </div>
        </div>
    </div>
</div>
@endsection
